import {StackNavigator} from 'react-navigation';
import ProductsListingPage from './components/ProductListing/ProductsListingPage';
import ProductDetailPage from './components/ProductDetail/ProductDetailPage';


export default StackNavigator(

  {
    ProductList: {
      screen: ProductsListingPage,
    },
    ProductDetail: {
      screen: ProductDetailPage,
    },
  },
  {
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
},
  {
    initialRouteName: 'ProductList',
  },



);
