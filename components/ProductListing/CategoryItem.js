import React from 'react';
import { StyleSheet,View,Image,Text } from 'react-native';

export default class CategoryItem extends React.Component {
  render() {
    return (
      <View style={styles.itemContainer}>
         <Image style={styles.image} source= {require('../../assets/icon.png')}/>
         <Text ellipsizeMode='tail' numberOfLines={1} style={styles.title}>{this.props.title}</Text>

      </View>
    );
  }
}

const styles = StyleSheet.create({

  itemContainer: {
    flexDirection:'column',
    width:200,
    height:140,
    marginRight:10,
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    justifyContent:'center',
    alignItems:'center'
  },
  image: {
    width:50,
    height: 50,
    resizeMode: 'contain',
  },

  title : {
    fontSize:15,
    color:'#000',
    margin:5,
  },

});
