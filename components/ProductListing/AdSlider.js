import React from 'react';
import { StyleSheet,Image,Dimensions} from 'react-native';
import SwiperFlatList from 'react-native-swiper-flatlist';

const imageNamesList = [require('../../assets/cat.jpeg'),require('../../assets/fox.jpeg'),require('../../assets/lion.jpeg')];
const indexedImage = index => ({image: imageNamesList[index%imageNamesList.length]});
const items = Array.from(Array(3)).map((_, index) => indexedImage(index));
const { width, height } = Dimensions.get('window');

export default class AdsPager extends React.Component {
  render() {
    return (
    <SwiperFlatList
     autoplay
     autoplayDelay={2}
     index={0}
     autoplayLoop
     autoplayInvertDirection
     data={items}
     renderItem={({ item }) => <Image style={styles.adItem} source={item.image} />}
     />
    );
  }
}

const styles = StyleSheet.create({
  adItem: {
      height: 200,
      width:width*0.9,
      marginRight:10
    },

  });
