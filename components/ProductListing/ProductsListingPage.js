import React from 'react';
import { StatusBar,StyleSheet, Text, View,FlatList,ScrollView } from 'react-native';
import ProductItem from './ProductItem';
import CustomToolBar from '../CustomToolBar';
import AdSlider from './AdSlider';
import CategoryItem from './CategoryItem';


const categories = [  {title: "Personal & Home Essentials"},
  {
      title: "Masks,Gloves & Protection"
  },

  {
        title: "Immunity Booster"
  },
  {
        title: "Immunity Booster 2"
  },
  {
        title: "Immunity Booster 3"
  },


];

const productsList = [
    {
      title: "Wildcraft Hypashield W95 Reusable Outdoor Protection Face Mask - Large",
      category: "Offers",
      subCategory: "Masks Sanitizer",
      price: "100",
      manufacturer: "Wildcraft India Pvt Ltd",
  },

  {
      title: "Ciphands Antiseptic Hand Sanitizer",
      category: "Beauty",
      subCategory: "Hands And Feet",
      price: "50",
      manufacturer: "Cipla Ltd(Otc)",

  },

  {
        title: "Mahalaxmi Enterprises 3 Ply Face Mask",
        category: "Covid Essentials",
        subCategory: "Masks and Gloves",
        price: "400",
        manufacturer: "Cipla Ltd(Otc)",
    },
    {
          title: "Mahalaxmi Enterprises 3 Ply Face Mask",
          category: "Covid Essentials",
          subCategory: "Masks and Gloves",
          price: "400",
          manufacturer: "Cipla Ltd(Otc)",
      },
      {
            title: "Mahalaxmi Enterprises 3 Ply Face Mask",
            category: "Covid Essentials",
            subCategory: "Masks and Gloves",
            price: "400",
            manufacturer: "Cipla Ltd(Otc)",
        },
        {
              title: "Mahalaxmi Enterprises 3 Ply Face Mask",
              category: "Covid Essentials",
              subCategory: "Masks and Gloves",
              price: "400",
              manufacturer: "Cipla Ltd(Otc)",
        },


];

export default class ProductsListingPage extends React.Component {

  listHeader = () => {
    return(
      <>
      <StatusBar backgroundColor="#20A0A1"/>
      <CustomToolBar />

      <View style={styles.greyContainer}>
          <Text style = {styles.categoryHeader}> Covid Essentials</Text>
          <AdSlider />
          <Text style = {styles.shopBy}>SHOP BY</Text>
          <Text style = {styles.categoryLabel}>Category</Text>

          <View style={styles.categoryContainer}>
               <FlatList horizontal={true} data={categories} renderItem={this.renderCatItem} />
          </View>
      </View>
      </>

    );
  };
  renderCatItem = ({ item }) => {
   return (
     <CategoryItem
        title =  {item.title}
     />
   );
  };

  renderProductItem = ({ item }) => {
   const { navigation: { navigate } } = this.props;
   return (
     <ProductItem
        title =  {item.title}
        category = {item.category}
        subCategory = {item.subCategory}
        price = {item.price}
        onPress={() => navigate('ProductDetail')}
        manufacturer = {item.manufacturer}
     />
   );
  };


  render() {
    return (
      <>


      <View style={styles.productsContainer}>
           <FlatList
             data={productsList}
             renderItem={this.renderProductItem}
             ListHeaderComponent = {this.listHeader}
          />
      </View>

    </>
    );
  }
}



const styles = StyleSheet.create({
  categoryHeader: {
    color: '#000',
    fontWeight: "bold",
    fontSize:18,
    padding:10
  },

  greyContainer: {
    flexDirection:'column',
    backgroundColor: '#F5F4F4',
    paddingBottom:30,
  },

  categoryContainer: {
    backgroundColor: '#F5F4F4',
    marginLeft:10,
    marginTop:10,
  },

  productsContainer: {
    backgroundColor: '#FFFFFF',
  },

  shopBy: {
    color: '#5C5E70',
    fontWeight: "bold",
    fontSize:14,
    marginLeft:10,
    marginTop:10,
  },
  categoryLabel: {
    color: '#000',
    fontSize:18,
    marginLeft:10
  },


});
