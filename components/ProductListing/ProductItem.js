import React from 'react';
import { StyleSheet,View,Image,Text,TouchableOpacity,TouchableHighlight } from 'react-native';

export default class ProductItem extends React.Component {
  render() {
    return (
      <TouchableHighlight
        onPress={this.props.onPress}>

      <View style={styles.rowContainer}>

         <Image style={styles.image} source= {require('../../assets/icon.png')}/>

         <View style = {styles.textContainer}>
             <Text style={styles.title}>{this.props.title}</Text>

             <View style = {styles.categoryContainer}>
                <Text style={styles.category}>{this.props.category}</Text>
                <Text style={styles.subcategory}>{this.props.subCategory}</Text>
             </View>

             <View style = {styles.priceContainer}>
                <Text style={styles.priceLabel}>Best Price</Text>
                <Text style={styles.priceValue}>{" ₹"+this.props.price}</Text>
             </View>

             <View style = {styles.manufacturerContainer}>
                <Text style={styles.manufacturer}>{"Mfr:"+this.props.manufacturer}</Text>
                <TouchableOpacity
                    style={styles.addCartButton}
                    activeOpacity = { .5 } >

                    <Text style={styles.addCartText}> ADD TO CART </Text>

               </TouchableOpacity>

             </View>

         </View>
      </View>
    </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({

  rowContainer: {
    height:190,
    flexDirection: "row",
    backgroundColor: '#FFFFFF',
    borderBottomColor: '#DDDDDD',
    borderBottomWidth: 1,
    paddingTop:10,
    paddingLeft:20,
  },

  textContainer:{
      flexDirection: "column",
      alignItems: 'flex-start',
      paddingHorizontal:20,
      flexShrink: 1,
  },

  categoryContainer:{
      flexDirection: "row",
      alignItems: 'flex-start',
      marginTop:5,
  },

  priceContainer:{
      flexDirection: "row",
      alignItems: 'flex-start',
      marginTop:8,
  },

  manufacturerContainer:{
      flexDirection: "row",
      alignItems: 'center',
      justifyContent:'space-between',

  },

  image: {
    width:60,
    height: 60,

  },

  title : {
    fontSize:17,
    color:'#000',
  },

  category : {
    fontSize:10,
    color:'#666',
    paddingHorizontal:10,
    paddingVertical:5,
    backgroundColor: '#F4F4F4',
    borderRadius: 5,
  },

  subcategory : {
    fontSize:10,
    color:'#666',
    marginLeft:20,
    paddingHorizontal:10,
    paddingVertical:5,
    backgroundColor: '#F4F4F4',
    borderRadius: 5,
  },

  priceLabel : {
    fontSize:16,
    color:'#5C5E6B',
    fontWeight: "bold"
  },

  priceValue : {
    fontSize:16,
    color:'#B04470',
    fontWeight: "bold"
  },

  manufacturer : {
    flex:1,
    fontSize:10,
    color:'#666',
  },

  addCartButton : {
    flex:1,
    height:35,
    alignItems:'center',
    justifyContent:'center',
    color:'#FF00FF',
    backgroundColor: '#1DC0CE',
    borderRadius: 20,
  },
  addCartText : {
    fontSize:12,
    color:'#FFF',
    fontWeight: "bold"
  },

});
