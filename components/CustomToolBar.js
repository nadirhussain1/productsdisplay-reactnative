import React from 'react';
import { StyleSheet,View,Image,Text,TouchableOpacity } from 'react-native';

export default class CustomToolBar extends React.Component {
  render() {
    return (
      <View style={styles.container}>
      <TouchableOpacity
          onPress={this.props.onPress}
          activeOpacity = { .5 } >
          <Image style={styles.backImage} source= {require('../assets/back_arrow.png')}/>
     </TouchableOpacity>

         <Image style={styles.logo} source= {require('../assets/netmeds_logo.png')}/>

          <View style={styles.searchCartContainer}>
              <Image style={styles.search} source= {require('../assets/search_icon.png')}/>
              <Image style={styles.cart} source= {require('../assets/cart_icon.png')}/>

          </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({

  container: {
    flexDirection: "row",
    backgroundColor: '#FFFFFF',
    height:60,
    justifyContent:'space-between',
    alignItems:'center',
    elevation:1,
  },

  backImage: {
    width:50,
    height:20,
    resizeMode: 'contain',
  },

  logo: {
    width:100,
    height:40,
    resizeMode: 'contain',
  },

  searchCartContainer:{
     flexDirection: 'row',
     justifyContent: 'flex-end',
     alignItems: 'center',
  },

  search: {
    width:30,
    height:30,
    resizeMode: 'contain',
    marginRight:10,
  },

  cart: {
    width:30,
    height:30,
    resizeMode: 'contain',
    marginRight:10,
  },




  });
