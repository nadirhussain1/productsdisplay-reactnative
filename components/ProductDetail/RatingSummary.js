import React from 'react';
import { StyleSheet,View,Image,Text,FlatList } from 'react-native';
import  {AirbnbRating}  from 'react-native-ratings';
import  RatingBarItem from './RatingBarItem';

const ratings = [
    {
      title: "5",
      percent: "100",
    },
    {
      title: "4",
      percent: "0",
    },
    {
      title: "3",
      percent: "0",
    },
    {
      title: "2",
      percent: "0",
    },
    {
      title: "1",
      percent: "0",
    },
];

export default class RatingSummary extends React.Component{
  render(){
      return(
          <View style={styles.rootContainer}>
             <Text style={styles.boldGreyText}>PRODUCT RATING</Text>
             <View style={styles.parentRow}>
                  <View style={styles.ratingColumn}>
                     <View flexDirection={'row'}>
                           <Text style={styles.darkRating}>5.0</Text>
                           <Text style={styles.greyRating}>/5</Text>
                     </View>
                     <AirbnbRating
                       count={5}
                       defaultRating={5}
                       size={15}
                       isDisabled={true}
                       showRating = {false}
                       selectedColor = {'#EA266D'}
                     />

                    <Text style={styles.basedOnText}>Based on 1 Ratings</Text>
                  </View>

                  <View>
                  <FlatList  data={ratings} renderItem={_ratingRenderItem} />
                </View>
             </View>
          </View>
      )
  }
}


_ratingRenderItem = ({ item }) => {
 return (
   <RatingBarItem
      title =  {item.title}
      percent =  {item.percent}
   />
 );
};


const styles = StyleSheet.create({
  rootContainer:{
      flexDirection: "column",
      backgroundColor:"#FFF",
      padding:15,
      borderRadius:5,
  },

  parentRow:{
      flex:1,
      flexDirection: "row",
      justifyContent:'space-around'
  },

  ratingColumn:{
      flexDirection: "column",
  },

  basedOnText : {
    fontSize:10,
    color:'#666',
    fontWeight:'bold',
  },
  darkRating : {
    fontSize:25,
    color:'#000',
    fontWeight:'bold',
  },
  greyRating : {
    fontSize:25,
    color:'#666',
    marginTop:5,

  },

});
