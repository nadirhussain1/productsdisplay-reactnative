import React from 'react';
import { StyleSheet,View,Image,Text } from 'react-native';

export default class RatingBarItem extends React.Component{
  render(){
      return(
          <View style={styles.rowContainer}>

            <Text style={styles.text}>{this.props.title}</Text>
            <Image style={styles.star} source={require('../../assets/grey_star.png')}/>
            <View style={styles.ratingLine} />
            <Text style={styles.text}>{this.props.percent+" %"}</Text>


          </View>
      )
  }
}

const styles = StyleSheet.create({

  rowContainer: {
    flexDirection: "row",
    alignItems:'center'
  },

  ratingLine: {
    width:100,
    flexDirection: "row",
    height: 5,
    marginLeft:5,
    backgroundColor:"#666",
    borderRadius:5,
  },

  text : {
    fontSize:12,
    color:'#666',
    fontWeight:'bold',
    marginLeft:5,
  },

  star : {
       width:15,
       height:15,
       resizeMode:'contain',
       marginLeft:5,
  },


});
