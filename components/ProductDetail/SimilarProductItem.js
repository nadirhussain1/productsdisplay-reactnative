import React from 'react';
import { StyleSheet,View,Image,Text,TouchableOpacity } from 'react-native';

export default class SimilarProductItem extends React.Component{
  render(){
      return(
          <View style={styles.rowContainer}>
            <Image style={styles.image} source={require('../../assets/icon.png')}/>
            <Text style={styles.title}>Hand Sanitizer 500ml</Text>


          </View>
      )
  }
}

const styles = StyleSheet.create({

  rowContainer: {
    width:150,
    height:150,
    marginRight:10,
    flexDirection: "column",
    backgroundColor: '#FFFFFF',
    borderColor: '#FFF',
    borderWidth: 1,
    borderRadius:5,
    justifyContent:'center',
    alignItems:'center'
  },

  image:{
    width:60,
    height:60,
  },

  title : {
    fontSize:12,
    color:'#000000',
    marginTop:10,
    paddingHorizontal:5,
    justifyContent:'flex-start'
  },



});
