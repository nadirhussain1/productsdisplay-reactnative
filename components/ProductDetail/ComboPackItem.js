import React from 'react';
import { StyleSheet,View,Image,Text,TouchableOpacity } from 'react-native';

export default class ComboPackItem extends React.Component{
  render(){
      return(
          <View style={styles.rowContainer}>
            <Image style={styles.image} source={require('../../assets/icon.png')}/>
            <Text style={styles.title}>{this.props.title}</Text>
            <Text style={styles.price}>{"MRP ₹ "+this.props.price}</Text>

            <TouchableOpacity
                style={styles.buyPackButton}
                activeOpacity = { .5 } >

                <Text style={styles.buyPackText}> BUY PACK </Text>

           </TouchableOpacity>
          </View>
      )
  }
}

const styles = StyleSheet.create({

  rowContainer: {
    width:150,
    height:200,
    marginRight:10,
    flexDirection: "column",
    backgroundColor: '#FFFFFF',
    borderColor: '#DDDDDD',
    borderWidth: 1,
    borderRadius:5,
    justifyContent:'center',
    alignItems:'center'
  },

  image:{
    width:40,
    height:40,
  },

  title : {
    fontSize:12,
    color:'#000000',
    marginTop:10,
  },

  price : {
    fontSize:12,
    color:'#B04470',
    marginTop:2,
    fontWeight:'bold',
  },

  buyPackButton : {
    width:100,
    height:35,
    alignItems:'center',
    justifyContent:'center',
    marginTop:10,
    backgroundColor: '#1DC0CE',
    borderRadius: 5,
  },

  buyPackText : {
    fontSize:12,
    color:'#FFF',
    fontWeight: "bold"
  },

});
