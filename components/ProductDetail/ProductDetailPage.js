import React from 'react';
import { StatusBar,StyleSheet, Text, View,Image,FlatList,TouchableOpacity,ScrollView } from 'react-native';
import CustomToolBar from '../CustomToolBar';
import ComboPackItem from './ComboPackItem';
import SimilarProductItem from './SimilarProductItem';
import RatingSummary   from './RatingSummary';
import RatingListItem   from './RatingListItem';

import {FlatListSlider} from 'react-native-flatlist-slider';

const images = [
  {
    image:require('../../assets/detail_image_1.jpeg'),
  },
  {
    image:require('../../assets/detail_image_2.jpeg'),
  },
  {
    image:require('../../assets/detail_image_3.jpeg'),
  },
  {
    image:require('../../assets/detail_image_2.jpeg'),
  },
];

const packsList = [
  {
    title: "Pack of 2",
    price: "100",
  },
  {
    title: "Pack of 3",
    price: "200",
  },
  {
    title: "Pack of 4",
    price: "300",
  },
];

const similarProductsList = [
  {
    title: "Hand Sanitizer 500ml",
  },
  {
    title: "MAGICA RUB HAND Sanitizer 100ml",
  },
  {
    title: "Hand Sanitizer 1000ml",
  },
];



export default class ProductDetailPage extends React.Component{

  upperScrollingSection = () => {
    return (
      <>
      <StatusBar backgroundColor="#20A0A1"/>
      <CustomToolBar onPress={() => this.props.navigation.pop()}/>


      <View style={styles.rootContainer}>
          <Text style = {styles.headerTitle}>Ciphands Antiseptic Hand Sanitizer 100ml</Text>
          <View style = {styles.catContainer}>
             <Text style = {styles.subCategory} numberOfLines={1} ellipsizeMode='tail' >Hand Washes/Sanitizer</Text>
             <View style={styles.likeShareContainer}>
                 <Image style={styles.likeShareIcon} source={require('../../assets/heart.png')}/>
                 <Image style={styles.likeShareIcon} source={require('../../assets/share.png')}/>
            </View>

         </View>

      <FlatListSlider
      data={images}
      local
      height={250}
      autoscroll={false}
      indicatorActiveColor={'#1BC0CE'}
      indicatorInActiveColor={'#DADADA'}
      indicatorActiveWidth={30}
      ndicatorInActiveWidth={30}
      />


      <View style = {styles.mfRowContainer}>
          <View style = {styles.mfColumnContainer}>
             <Text style={styles.smallTextGrey}>{"Mfr: Cipla Ltd(Otc)"}</Text>
             <Text style={styles.smallTextGrey}>{"Country of Origin: India"}</Text>
          </View>

          <TouchableOpacity
             style={styles.addCartButton}
             activeOpacity = { .5 } >
             <Text style={styles.addCartText}> ADD TO CART </Text>
          </TouchableOpacity>

      </View>


      <View style = {styles.priceContainer}>
          <Text style={styles.priceLabel}>Best Price </Text>
          <Text style={styles.priceValue}>{" ₹ 50.00"}</Text>
      </View>

      <Text style={styles.smallTextGrey}>(Inclusive of all taxes)</Text>
      <Text style={styles.smallTextGrey}>* Delivery charges if applicable will be applied at checkout</Text>

      <View style = {styles.deliveryContainer}>
            <Text style={styles.smallTextDark}>Expected Delivery: Tomorrow </Text>
            <Text style={styles.delivery}>1 day delivery</Text>
      </View>

      <View style = {styles.divContainer}/>


      <View style = {styles.expiryInfoRootContainer}>
         <View style = {styles.expiryDateContainer}>
             <Text style={styles.smallTextGrey}>Expiry Date</Text>
             <Text style={styles.smallTextDark}>Apr 2023</Text>
         </View>

        <View style = {styles.expiryPinContainer}>
           <Text style={styles.smallTextGrey}>Pincode</Text>
           <View flexDirection='row'>
               <Text style={styles.smallTextDark}>400001</Text>
               <Text style={styles.changePin}>Change</Text>
            </View>
        </View>

        <View style = {styles.expiryPinContainer}/>

      </View>

      </View>

      <View style={styles.greySectionContainer}>

          <View style={styles.comboPacksContainer}>
            <Text style={styles.boldGreyText}>COMBO PACKS</Text>
            <FlatList marginTop={5} horizontal={true} data={packsList} renderItem={this.packsListRenderItem} />
         </View>

         <Text  style={styles.similarProducts}>SIMILAR PRODUCTS</Text>
         <FlatList marginTop={5} horizontal={true} data={packsList} renderItem={this.similarRenderItem} />

         <View style={styles.productDetailsContainer}>
           <Text  style={styles.boldGreyText}>PRODUCT DETAILS</Text>
        </View>

        <View style={styles.countryContainer}>
            <Text  style={styles.medTextGrey}>Country of Origin</Text>
            <Text  style={styles.medTextGrey}>India</Text>
        </View>

        <View style={styles.disclaimerContainer}>
              <Text style={styles.boldGreyText}>Disclaimer</Text>
              <Text style={styles.smallTextGrey}>
                 The content of this website are for informational purposes only and not intended to be a substitute for professional medical advice.
             </Text>
         </View>

        <View style={styles.ratingContainer}>
             <RatingSummary />
         </View>

      </View>
      </>
    );
  };
  packsListRenderItem = ({ item }) => {
    return (
      <ComboPackItem
      title =  {item.title}
      price = {item.price}
      />
    );
  };

  similarRenderItem = ({ item }) => {
    return (
      <SimilarProductItem
      title =  {item.title}
      />
    );
  };

  ratingListRenderItem = ({ item }) => {
    return (
      <RatingListItem />
    );
  };

  render(){
    return(
      <>
      <FlatList
      data={packsList}
      renderItem={this.ratingListRenderItem}
      ListHeaderComponent = {this.upperScrollingSection}
      />
      </>
    );
  }
}






const styles = StyleSheet.create({
  rootContainer: {
    flexDirection: 'column',
    flex:1,
    paddingHorizontal:16,
  },

  catContainer: {
    flexDirection: 'row',
    height:50,
    justifyContent:'space-between',
    alignItems:'center',
    marginTop:10,
  },

  likeShareContainer: {
    width:60,
    flexDirection: 'row',
    justifyContent:'space-between',
    marginTop:20,
  },
  mfRowContainer:{
    flexDirection: "row",
    alignItems: 'center',
    marginTop:30,
    justifyContent:'space-between',
  },
  mfColumnContainer:{
    flexDirection: "column",
  },
  priceContainer:{
    flexDirection: "row",
    alignItems: 'flex-start',
    marginTop:30,
  },

  deliveryContainer:{
    flexDirection: "row",
    height:40,
    alignItems:'center',
    paddingHorizontal:10,
    justifyContent: 'space-between',
    backgroundColor:"#FFEBF3",
    marginTop:10,
  },
  deliveryContainer:{
    flexDirection: "row",
    height:40,
    alignItems:'center',
    paddingHorizontal:10,
    justifyContent: 'space-between',
    backgroundColor:"#FFEBF3",
    marginTop:10,
  },
  divContainer:{
    flexDirection: "row",
    height:1,
    marginTop:10,
    backgroundColor:"#D3D3D3"
  },
  expiryInfoRootContainer:{
    flexDirection: "row",
    height:100,
    justifyContent: 'space-between',
    marginTop:10,
  },
  expiryDateContainer:{
    flexDirection: "column",
  },
  expiryPinContainer:{
    flexDirection: "column",
  },

  greySectionContainer:{
    flexDirection: "column",
    backgroundColor:"#F4F4F4",
    padding:16,
  },

  comboPacksContainer:{
    flexDirection: "column",
    backgroundColor:"#FFF",
    padding:15,
    borderRadius:5,
  },

  productDetailsContainer:{
    flexDirection: "row",
    backgroundColor:"#FFF",
    paddingHorizontal:10,
    height:40,
    alignItems:'center',
    borderTopLeftRadius:5,
    borderTopRightRadius:5,
    marginTop:20,

  },

  countryContainer:{
    flexDirection: "row",
    padding:15,
    justifyContent:'space-between'
  },

  disclaimerContainer:{
    flexDirection: "column",
    backgroundColor:"#FFF",
    padding:15,
    borderRadius:5,
  },
  ratingContainer:{
    flexDirection: "column",
    marginTop:20,
  },




  likeShareIcon: {
    width: 20,
    height:20,
    resizeMode:'contain'
  },

  headerTitle: {
    fontSize: 22,
    fontWeight:'bold',
    marginTop:8
  },

  subCategory: {
    width:120,
    height:30,
    justifyContent:'center',
    paddingLeft:10,
    fontSize: 14,
    color:'#616271',
    backgroundColor:'#F5F4F4',
    borderRadius:5,
  },
  addCartButton : {
    width:120,
    height:35,
    alignItems:'center',
    justifyContent:'center',
    color:'#FF00FF',
    backgroundColor: '#1DC0CE',
    borderRadius: 10,
  },
  addCartText : {
    fontSize:12,
    color:'#FFF',
    fontWeight: "bold"
  },
  priceLabel : {
    fontSize:16,
    color:'#5C5E6B',
    fontWeight: "bold"
  },

  priceValue : {
    fontSize:16,
    color:'#B04470',
    fontWeight: "bold"
  },

  smallTextGrey : {
    fontSize:10,
    color:'#666',
  },
  smallTextDark : {
    fontSize:10,
    color:'#000',
  },
  medTextGrey : {
    fontSize:13,
    color:'#666',
  },
  delivery : {
    fontSize:10,
    color:'#B04470',
  },
  changePin : {
    fontSize:12,
    color:'#B04470',
    marginLeft:5
  },

  boldGreyText : {
    fontSize:10,
    color:'#666',
    fontWeight:'bold',
  },

  similarProducts : {
    fontSize:10,
    color:'#666',
    fontWeight:'bold',
    marginTop:30,
  },



});
