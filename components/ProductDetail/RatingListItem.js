import React from 'react';
import { StyleSheet,View,Image,Text } from 'react-native';
import  {AirbnbRating}  from 'react-native-ratings';


export default class RatingListItem extends React.Component{
  render(){
      return(
          <View style={styles.rootContainer}>
            <View style={styles.topRowContainer}>
               <Text style={styles.boldGreyText}>PRODUCT RATING</Text>
               <View flexDirection={'row'}>
                     <Text style={styles.darkRating}>5.0</Text>
                     <Text style={styles.greyRating}>/5</Text>
               </View>
            </View>

            <View style={styles.secondRowContainer}>
               <AirbnbRating
                 count={5}
                 defaultRating={5}
                 size={15}
                 isDisabled={true}
                 showRating = {false}
                 selectedColor = {'#EA266D'}
                   />

                <Text style={styles.postDate}>PrashantVerma Posted on 02/05/2020</Text>

            </View>

            <Text style={styles.reviewTitle}>Good enough</Text>
            <Text style={styles.reviewComment}>Its perfect and necessary to use</Text>


          </View>
      )
  }
}

const styles = StyleSheet.create({
  rootContainer: {
    flexDirection: "column",
    backgroundColor:'#FFF',
    borderRadius:10,
    margin:15,
    padding:15,
  },
  topRowContainer: {
    flexDirection: "row",
    justifyContent:'space-between'
  },
  secondRowContainer: {
    flexDirection: "row",
    justifyContent:'space-between',
    marginTop:30,
  },

  boldGreyText : {
    fontSize:15,
    color:'#666',
  },
  darkRating : {
    fontSize:12,
    color:'#000',
    fontWeight:'bold',
  },
  greyRating : {
    fontSize:12,
    color:'#666',

  },
  postDate : {
    fontSize:10,
    color:'#666',
    fontStyle:'italic'
  },
  reviewTitle : {
    fontSize:12,
    color:'#000',
    fontWeight:'bold',
    marginTop:10,
  },
  reviewComment : {
    fontSize:10,
    color:'#666',
    fontStyle:'italic',
    marginTop:15,
  }


});
